<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ScientificDegree */

$this->title = Yii::t('app', 'Create Scientific Degree');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Scientific Degrees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scientific-degree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
