<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request`.
 */
class m180422_091548_create_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'specialization_id' => $this->integer()->notNull(),
            'scientific_degree_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'surname' => $this->string(255)->notNull(),
            'patronymic' => $this->string(255)->null(),
            'email' => $this->string(255)->notNull(),
            'desc' => $this->text()->null(),
            'date' => $this->dateTime()->notNull(),
            'paid' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey('request_specialization', 'request', 'specialization_id', 'specialization', 'id',
            'RESTRICT', 'RESTRICT');
        $this->addForeignKey('request_scientific_degree', 'request', 'scientific_degree_id', 'scientific_degree', 'id',
            'RESTRICT', 'RESTRICT');

        $this->createIndex('idx_request_specialization', 'request', 'specialization_id');
        $this->createIndex('idx_request_scientific_degree', 'request', 'scientific_degree_id');
        $this->createIndex('idx_request_email', 'request', 'email');
        $this->createIndex('idx_request_date', 'request', 'date');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_request_specialization', 'request');
        $this->dropIndex('idx_request_scientific_degree', 'request');
        $this->dropIndex('idx_request_email', 'request');
        $this->dropIndex('idx_request_date', 'request');

        $this->dropForeignKey('request_specialization', 'request');
        $this->dropForeignKey('request_scientific_degree', 'request');

        $this->dropTable('request');
    }
}
