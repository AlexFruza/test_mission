<?php

use yii\db\Migration;

/**
 * Handles the creation of table `specialization`.
 */
class m180422_091119_create_specialization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('specialization', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->text()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('specialization');
    }
}
