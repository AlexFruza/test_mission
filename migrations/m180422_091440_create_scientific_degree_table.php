<?php

use yii\db\Migration;

/**
 * Handles the creation of table `scientific_degree`.
 */
class m180422_091440_create_scientific_degree_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('scientific_degree', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'desc' => $this->text()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('scientific_degree');
    }
}
