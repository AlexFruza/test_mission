<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $specialization_id
 * @property int $scientific_degree_id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property string $desc
 * @property string $date
 * @property bool $paid
 *
 * @property ScientificDegree $scientificDegree
 * @property Specialization $specialization
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['specialization_id', 'scientific_degree_id', 'name', 'surname', 'email', 'date'], 'required'],
            [['specialization_id', 'scientific_degree_id'], 'default', 'value' => null],
            [['specialization_id', 'scientific_degree_id'], 'integer'],
            [['desc'], 'string'],
            [['date'], 'safe'],
            [['paid'], 'boolean'],
            [['name', 'surname', 'patronymic', 'email'], 'string', 'max' => 255],
            [
                ['scientific_degree_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ScientificDegree::className(),
                'targetAttribute' => ['scientific_degree_id' => 'id']
            ],
            [
                ['specialization_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Specialization::className(),
                'targetAttribute' => ['specialization_id' => 'id']
            ],
            ['date', function ($attribute, $params) {
                $data = new \DateTime($this->$attribute);
                $currentData = new \DateTime();
                if ($data <= $currentData) {
                    $this->addError($attribute, 'Дата приема должна быть больше текущей.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'specialization_id' => Yii::t('app', 'Specialization ID'),
            'scientific_degree_id' => Yii::t('app', 'Scientific Degree ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'patronymic' => Yii::t('app', 'Patronymic'),
            'email' => Yii::t('app', 'Email'),
            'desc' => Yii::t('app', 'Desc'),
            'date' => Yii::t('app', 'Date'),
            'paid' => Yii::t('app', 'Paid')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScientificDegree()
    {
        return $this->hasOne(ScientificDegree::className(), ['id' => 'scientific_degree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasOne(Specialization::className(), ['id' => 'specialization_id']);
    }
}
