<?php

namespace app\controllers;

use yii\rest\ActiveController;

class RestRequestController extends ActiveController
{
    public $modelClass = 'app\models\Request';
}